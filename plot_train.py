import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


if __name__ == '__main__':

    data_noob = [[], []]
    data_score = [[], []]
    pid = None
    if len(sys.argv) > 1:
        fn = sys.argv[1]
    else:
        fn = './result.txt'

    if len(sys.argv) > 2:
        output = sys.argv[2]
    else:
        output = fn + '.png'

    with open(fn, 'r') as f:
        for line in f.readlines():
            # pid
            if line.startswith('* agent0'):
                pid = 0
            if line.startswith('* agent1'):
                pid = 1

            if line.startswith(' - Noobs:'):
                v = line.split(':')[1]
                data_noob[pid].append(float(v))

            if line.startswith(' - Score:'):
                v = line.split(':')[1]
                data_score[pid].append(float(v))

    plt.clf()
    
    plt.plot(np.arange(len(data_noob[0])),
             data_noob[0], linestyle='dashed', color='blue')    
    plt.plot(np.arange(len(data_noob[1])) + 0.5,
             data_noob[1], linestyle='dashed',  color='red')
    plt.plot(np.arange(len(data_score[0])), data_score[0], color='blue')
    plt.plot(np.arange(len(data_score[1])) + 0.5, data_score[1], color='red')
    
    plt.xlabel('Training Times')
    plt.ylabel('Agent Scores')
    plt.grid()
    plt.axis([None, None, 0.0, 1.0])
    plt.legend(('noob0', 'noob1', 'score0', 'score1'))
    
    plt.savefig(output)
    