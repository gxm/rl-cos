for ((i=0; i<8; ++i)); do
    rm -r "train$i"
    mkdir "train$i"
    mkdir "train$i/agent"
    cp *.py "train$i"
done

for ((i=0; i<8; ++i)); do
    echo "`date`: Train $i start." && \
    cd "train$i" && \
    python agent_train.py --init "../best/agent0-v$i.npy" "../best/agent1-v$i.npy" >> result.txt && \
    echo "`date`: Train $i finished." &
    sleep 30
done

wait

echo "`date`: Select best agents"
python agent_select.py 'train*/'
echo "`date`: done. Save to best."
for ((i=0; i<8; ++i)); do
    mv "train$i/result.txt" "best/result-$i.txt"
done

mv map.npy best/
mv map.csv best/
cp best/agent0-v0.npy best/agent0.npy
cp best/agent0-v1.npy best/agent1.npy

echo -n "total agents: "
grep 'Score' best/result-*.txt | wc -l
for ((i=0; i<9; ++i)); do
    echo -n "agents in 0.$i ~ 0.$((i+1)): "
    grep "Score: 0.$i" best/result-*.txt | wc -l
done
