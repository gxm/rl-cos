from agent_np import AgentTrained as AgentTrainedNP
from agent import AgentTrained
from agent import AgentRand


import os
from env import CardofSpirits
import numpy as np

game = CardofSpirits()
p0 = AgentRand()
p1 = AgentTrained(game, "agent1-v0.npy", 1)
p2 = AgentTrainedNP(game, "agent1-v0.npy", 1)

game.reset()
for i in range(100):
    game.render(False)
    if game.result is None:
        if game.turn % 2 == 0:
            cid = p0.select_cid_best(game.state, game.choices)
        else:
            q1 = p1.run_target(game.state.reshape(1, -1))[0]
            cid1 = p1.select_cid_best(game.state, game.choices)
            q2 = p2.run_target(game.state.reshape(1, -1))[0]
            cid2 = p2.select_cid_best(game.state, game.choices)
            print("tf: ", cid1, np.array2string(
                q1[game.choices], formatter={'float_kind': lambda x: "%.2f" % x}))
            print("np: ", cid2, np.array2string(
                q2[game.choices], formatter={'float_kind': lambda x: "%.2f" % x}))
            cid = cid1
        game.use(cid)
    else:
        break
