# Agents for card of spirits

## introduction

Use reinforcement learning to train 2 agents: first and back.

Use dueling + double DQN.

Agent will play with another agent. After train and learn, agent who has a score over 0.5 (compete with rand) will be saved.

Make a full compete of all saved agents, sort them.

## train yourself agents

use PBS:

```bash
qsub run.sh
```

no parallel script:

```bash
python agent_train.py > ./result.txt
python agent_select.py . 16
python human.py -a0 best/agent0.npy -a1 best/agent1.npy
```

## human.py

Play with trained agents, help_agent.npy can be empty:

```bash
python human.py -h0 help_agent.npy -a1 agent1.npy
python human.py -h1 help_agent.npy -a0 agent1.npy
```

Agents play with rand/another agent:

```bash
python human.py -a0 agent1.npy -a1 agent2.npy
python human.py -a0 agent1.npy -a1 agent2.npy -n 10000
```

## run in server

First make a directory and cd into it:

```bash
git clone https://gitlab.com/gxm/rl-cos.git --depth=1 .
rm -rf .git
qsub run.sh

```

## check train state

```bash
grep 'Score: ' train*/result.txt | wc -l
grep 'Score: 0.5' train*/result.txt | wc -l
grep 'Score: 0.4' train*/result.txt | wc -l
grep 'Score: 0.3' train*/result.txt | wc -l

```

## best agents

See this snippets:

- [best-0.5.3-2018/10/15](https://gitlab.com/gxm/rl-cos/snippets/1763496)
- [best-0.5.4-2018/10/21](https://gitlab.com/gxm/rl-cos/snippets/1766594)
- [best-0.5.5-2018/10/27](https://gitlab.com/gxm/rl-cos/snippets/1770178)
- [best-0.6.1-2018/11/22](https://gitlab.com/gxm/rl-cos/snippets/1782470)

