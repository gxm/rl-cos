import numpy as np

# DQN Setting
HiddenLayer_Size = 256
HiddenLayer_Depth = 4


class AgentTrained:
    def __init__(self, game, load_file, a_id=0):
        layers = [[32, 256], [256, 256], [256, 256],
                  [256, 256], [256, 1], [256, 13]]
        self.load(load_file, layers)

    def run_target(self, state):
        a = state.reshape(1, -1)
        for i in range(4):
            a = np.matmul(a, self.w[i]) + self.b[i]
            a[a <= 0] = 0
        val = np.matmul(a, self.w[4]) + self.b[4]
        adv = np.matmul(a, self.w[5]) + self.b[5]
        return val + adv - np.mean(adv)

    def load(self, load_file, layers):
        weights = np.load(load_file)

        self.w, self.b = [], []
        k = 0
        for row, col in layers:
            self.w.append(weights[0, k:k+row*col].reshape(row, col))
            k += row * col
            row = 1
            self.b.append(weights[0, k:k+row*col].reshape(row, col))
            k += row * col

    def select_cid_best(self, state, choices):
        q = self.run_target(state.reshape(1, -1))
        return choices[np.argmax(q[0][choices])]


class AgentRand:
    def __init__(self):
        pass

    def select_cid(self, state, choices):
        return np.random.choice(choices)

    def select_cid_best(self, state, choices):
        return self.select_cid(state, choices)


class AgentHuman:
    """AgentHuman
    Human play vs agent
    """

    def __init__(self, game, help_agent=None):
        print("Human mode.")
        cards = game.Card_Name
        self.name2id = dict(zip(cards, range(len(cards))))
        if help_agent is not None:
            self.help_agent = AgentTrained(game, help_agent, -1)
        else:
            self.help_agent = None

    def select_cid_best(self, state, choices):
        if self.help_agent is not None:
            q = self.help_agent.run_target(state.reshape(1, -1))
            print('rewards:', np.round(q[0][choices] * 100) / 100)
        while True:
            a = input('Select: ').upper()
            if a in self.name2id.keys():
                cid = self.name2id[a]
                if cid in choices:
                    return cid
