from ddqn import DuelingDQN as DQNModel
import numpy as np
import tensorflow as tf

# DQN Setting
HiddenLayer_Size = 256
HiddenLayer_Depth = 4

# Sigmoid select temperature
# t -> 0, choose max, t -> oo choose random
Sigmoid_T = 0.1


class AgentDQN(DQNModel):
    def __init__(self, game):
        # train parameters
        self.reward_decay = 0.95
        self.learning_rate = 0.0001
        self.e_greedy = 0.30
        self.memory_size = 1000
        self.batch_size = 64
        self.replace_iteration = 100
        self.learn_iteration = 5

        self.n_layer_input = game.Spaces
        self.n_layer_output = game.Actions
        self.n_layer_hidden = HiddenLayer_Size
        self.n_depth_hidden = HiddenLayer_Depth
        self._build_net()

        self.reset_memory()
        self.cost_his = []

    def reset_memory(self):
        self.memory = np.zeros((self.memory_size, self.n_layer_input * 2 + 3))
        self.learn_counter = 0
        self.memory_counter = 0

    def select_cid(self, state, choices, e_greedy=None):
        if e_greedy is None:
            e_greedy = self.e_greedy
        if np.random.rand() < e_greedy:
            q_target = self.run_target(state.reshape(1, -1))
            return np.argmax(q_target)
        else:
            return self.select_cid_best(state, choices)

    def select_cid_best(self, state, choices):
        q_target = self.run_target(state.reshape(1, -1))
        return choices[np.argmax(q_target[0][choices])]

    def select_cid_opponent(self, state, choices):
        q_opponent = self.run_opponent(state.reshape(1, -1))
        if Sigmoid_T is None:
            return choices[np.argmax(q_opponent[0][choices])]
        else:
            w = np.exp(q_opponent[0][choices] / Sigmoid_T)
            return np.random.choice(choices, p=w / np.sum(w))

    def learn(self, train_data):
        for state in train_data:
            self.memory[self.memory_counter % self.memory_size] = state
            self.memory_counter += 1

        if self.memory_counter < self.memory_size:
            return

        self.learn_counter += 1

        if self.learn_counter % self.learn_iteration != 0:
            return
        # replace target network with eval network
        if self.learn_counter % self.replace_iteration == 0:
            self.run_replace()

        sample_index = np.random.choice(self.memory_size, size=self.batch_size)
        batch_memory = self.memory[sample_index, :]
        cost = self.run_train(batch_memory)
        self.cost_his.append(cost)

    def plot_cost(self):
        import matplotlib
        matplotlib.use('Agg')
        import matplotlib.pyplot as plt
        plt.clf()
        plt.plot(np.arange(len(self.cost_his)), self.cost_his)
        plt.ylabel('Cost')
        plt.xlabel('training steps')
        plt.savefig('cost.png')


class AgentRand:
    def __init__(self):
        pass

    def select_cid(self, state, choices):
        return np.random.choice(choices)

    def select_cid_best(self, state, choices):
        return self.select_cid(state, choices)


class AgentTrained(DQNModel):
    Weight_Cache = {}  # class variable, shared with all Trained Agents

    def __init__(self, game, load_file, a_id=0):
        self.n_layer_input = game.Spaces
        self.n_layer_output = game.Actions
        self.n_layer_hidden = HiddenLayer_Size
        self.n_depth_hidden = HiddenLayer_Depth
        self.name = 'trained_%d' % a_id
        self.tf_scope = 'q_' + self.name
        self._build_net()
        self.load(load_file)

    def _build_net(self):
        self.s = tf.placeholder(
            tf.float32, [None, self.n_layer_input], name='s_' + self.name)
        self.new = tf.placeholder(tf.float32, shape=[1, None])
        self.q_target = self._build_net_single(self.tf_scope)
        self.sess = tf.Session()

    def select_cid_best(self, state, choices):
        q_target = self.run_target(state.reshape(1, -1))
        return choices[np.argmax(q_target[0][choices])]

    def load(self, load_file=None):
        if load_file is not None:
            if load_file in self.Weight_Cache.keys():
                weights = self.Weight_Cache[load_file]
            else:
                weights = np.load(load_file)
                self.Weight_Cache[load_file] = weights

            self.np2vars(self.tf_scope, weights)


class AgentTrainedNP:
    def __init__(self, game, load_file, a_id=0):
        layers = [game.Spaces] + [HiddenLayer_Size] * \
            (HiddenLayer_Depth) + [game.Actions]
        self.load(load_file, layers)

    def run_target(self, state):
        a = state.reshape(1, -1)
        for i in range(HiddenLayer_Depth + 1):
            a = np.matmul(a, self.w[i]) + self.b[i]
            a[a <= 0] = 0
        return a

    def load(self, load_file, layers):
        weights = np.load(load_file)

        self.w, self.b = [], []

        k = 0
        for i in range(HiddenLayer_Depth + 1):
            row, col = layers[i], layers[i+1]
            self.w.append(weights[0, k:k+row*col].reshape(row, col))
            k += row * col
            row, col = 1, layers[i+1]
            self.b.append(weights[0, k:k+row*col].reshape(row, col))
            k += row * col

    def select_cid_best(self, state, choices):
        np.random.shuffle(choices)
        q = self.run_target(state.reshape(1, -1))
        return choices[np.argmax(q[0][choices])]


class AgentHuman:
    """AgentHuman
    Human play vs agent
    """

    def __init__(self, game, help_agent=None):
        print("Human mode.")
        cards = game.Card_Name
        self.name2id = dict(zip(cards, range(len(cards))))
        if help_agent is not None:
            self.help_agent = AgentTrained(game, help_agent, -1)
        else:
            self.help_agent = None

    def select_cid_best(self, state, choices):
        if self.help_agent is not None:
            q = self.help_agent.run_target(state.reshape(1, -1))
            print('rewards:', np.round(q[0][choices] * 100) / 100)
        while True:
            a = input('Select: ').upper()
            if a in self.name2id.keys():
                cid = self.name2id[a]
                if cid in choices:
                    return cid
