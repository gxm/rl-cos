import json
import numpy as np
import pandas as pd


class CardofSpirits:
    Version = "0.6.2"
    """Card of Spirits v0.6.2
    cards  : [0, 1, 2, 3, 4, 5, 6, 7, 8, J, Q, K, X]
    numbers: [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3] = total 39
    rules:
    1. The game has [2] players.
    2. Every player has [9] cards in hand, and other [16] cards in deck. The last [4] cards just dropped.
    3. Players take turns to play [1] card.
    4. The score of a player is the sum of all cards in field expect the covered one.
    5. If one player cannot play any card in his turn:
        5.1 He losts if he has any cards in hand.
        5.2 Draw if he has no card in hand.
    6. After playing a card, if there's equal score, clear the field and both draw [1] card.

    cards:
    0 | opponent can only play normal cards (1-8) in the next turn
      | can not use when the fields is clear
    1 | can enable the covered card
      | used as normal card if there's not a covered card
      | used as normal card if last play is 0
    N | normal cards include 1-8
      | can only be used if ( card number >= opponent's score - your score )
    J | clear the fields
      | can not use when the fields is clear
    Q | exchange fields
      | can not use when the fields is clear
    K | Cover the first card in opponent's field
      | can not use when opponent's fields is clear
      | remove the covered card before cover the top when there's a covered card
    X | Use as the opponent's last play
      | cannot copy X itself

    update:
    [0.6.2]: K can be used when there's a covered card.
    [0.6.1]: Set all cards' number as 3
    [0.6]: Add card X.
    [0.5.5]: Send the infomation of trimed cards to agent.
    """

    Card_Name = ["0", "1", "2", "3", "4", "5",
                 "6", "7", "8", "J", "Q", "K", "X"]
    Card_Deck = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3]
    Card_Value = [0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0]
    Init_CardNum = 9
    Trim_CardNum = 3

    Actions = len(Card_Deck)
    Spaces = Actions * 2 + 6  # remain + hands + info(6)

    def __init__(self):
        print("Welcome to play Card of Spirits!")
        print("Version:", self.Version)
        np.random.seed()
        self.reset()

    def reset(self):
        self.cards = []
        for cid, number in enumerate(self.Card_Deck):
            self.cards += [cid] * number

        np.random.shuffle(self.cards)

        self.cards_trimed = self.cards[-self.Trim_CardNum:]
        self.cards = self.cards[0:-self.Trim_CardNum]

        self.remain = self.Card_Deck.copy()
        for cid in self.cards_trimed:
            self.remain[cid] -= 1

        self.fields = [[], []]
        self.bans = [0, 0]
        self.hands = [[0] * self.Actions, [0] * self.Actions]

        self.turn = 0
        self.last_play = -1

        self.draw(self.Init_CardNum, pid=0)
        self.draw(self.Init_CardNum, pid=1)

        self.records = []
        self.noob = None

        max_cache = sum(self.Card_Deck)
        self.cache = {}
        for key in ['tops', 'scores', 'state', 'choices', 'result']:
            self.cache[key] = [None] * max_cache

    def render(self, hide=None):
        print("turn:%2d" % self.turn)
        data = []
        for pid in range(2):
            row = []
            row.append(self.scores[pid])

            hand = "".join(
                [
                    self.Card_Name[cid] * number
                    for cid, number in enumerate(self.hands[pid])
                ]
            )

            if hide != pid:
                row.append(hand)
            else:
                row.append('?' * len(hand))

            field = "".join([self.Card_Name[cid] for cid in self.fields[pid]])
            row.append(field)

            if self.bans[pid] == 0:
                ban = ""
            else:
                ban = self.Card_Name[self.bans[pid]]
            row.append(ban)

            if self.tops[pid] == -1:
                top = ""
            else:
                top = self.Card_Name[self.tops[pid]]
            row.append(top)

            if self.turn % 2 == pid:
                play = "*"
            elif self.last_play == -1:
                play = ""
            else:
                play = self.Card_Name[self.last_play]
            row.append(play)

            data.append(row)

        df = pd.DataFrame(
            data,
            index=["p0", "p1"],
            columns=["s", "hand", "field", "ban", "top", "play"],
        )
        print(df)
        if self.turn % 2 ^ 1 == hide:
            print("choices: ", [self.Card_Name[cid] for cid in self.choices])

    @property
    def tops(self):
        key = 'tops'
        if self.cache[key][self.turn] is None:
            self.cache[key][self.turn] = self._tops()

        return self.cache[key][self.turn]

    def _tops(self):
        tops0 = -1 if len(self.fields[0]) == 0 else self.fields[0][-1]
        tops1 = -1 if len(self.fields[1]) == 0 else self.fields[1][-1]

        return [tops0, tops1]

    @property
    def scores(self):
        scores = [0, 0]
        for pid in range(2):
            for cid in self.fields[pid]:
                scores[pid] += self.Card_Value[cid]

        return scores

    @property
    def state(self):
        key = 'state'
        if self.cache[key][self.turn] is None:
            self.cache[key][self.turn] = self._state()

        return self.cache[key][self.turn]

    def _state(self, pid=None):
        if pid is None:
            pid = self.turn % 2
        tops = self.tops
        scores = self.scores
        info = [
            tops[pid],
            tops[1 - pid],
            self.bans[pid],
            self.bans[1 - pid],
            scores[1 - pid] - scores[pid],
            self.last_play,
        ]
        return np.array(self.remain + self.hands[pid] + info)

    @property
    def choices(self):
        key = 'choices'
        if self.cache[key][self.turn] is None:
            self.cache[key][self.turn] = self._choices()

        return self.cache[key][self.turn]

    def _choices(self):
        pid = self.turn % 2
        # normal cards
        scores = self.scores
        dscore = (scores[1] - scores[0]) * (1 - 2 * pid)
        choices_rule = [i + 1 for i in range(8) if i + 1 >= dscore]
        # special cards
        if self.last_play != 0:
            if self.bans[pid] != 0:
                choices_rule.append(1)            
            if self.tops[0] != -1 or self.tops[1] != -1:
                choices_rule += [0, 9, 10]            
            if self.tops[1 - pid] != -1:
                choices_rule.append(11)
        # X, cid == 12
        if self.last_play in choices_rule:
            choices_rule.append(12)
        # remove choices
        choices = [cid for cid, num in enumerate(self.hands[pid])
                   if num > 0 and cid in choices_rule]

        return choices

    def draw(self, number, pid):
        for cid in self.cards[0:number]:
            self.hands[pid][cid] += 1
        self.cards = self.cards[number:]

    def use(self, cid):
        pid = self.turn % 2
        # play a wrong card
        if cid not in self.choices:
            self.noob = pid
            return
        # hand cards
        self.hands[pid][cid] -= 1
        # remain cards
        self.remain[cid] -= 1
        # last play
        if cid == 12:
            cid, self.last_play = self.last_play, cid
        else:
            self.last_play = cid

        # take effects
        # 1. use 1 as cure
        if cid == 1 and self.bans[pid] != 0:
            x = self.bans[pid]
            self.fields[pid].append(x)
            self.bans[pid] = 0
        # 2. use normal card
        elif self.Card_Value[cid] > 0:
            self.fields[pid].append(cid)
            self.bans[pid] = 0
        # 3. J(9), Q(10), K(11)
        else:
            if cid == 9:
                self.fields[0].clear()
                self.fields[1].clear()
                self.bans = [0, 0]

            if cid == 10:
                self.fields[0], self.fields[1] = self.fields[1], self.fields[0]
                self.bans[0], self.bans[1] = self.bans[1], self.bans[0]

            if cid == 11:
                x = self.fields[1 - pid].pop()
                self.bans[1 - pid] = x

        # if get same score
        if self.scores[0] == self.scores[1]:
            self.fields[0].clear()
            self.fields[1].clear()
            self.bans = [0, 0]
            self.draw(1, pid=0)
            self.draw(1, pid=1)

        # turn
        self.turn += 1

    def use_t(self, cid):
        # record
        self.records.append([cid, self.state])
        self.use(cid)

    # result is draw if both hands empty
    # else who plays last turn wins
    @property
    def result(self):
        key = 'result'
        if self.cache[key][self.turn] is None:
            self.cache[key][self.turn] = self._result()

        return self.cache[key][self.turn]

    def _result(self):
        if self.noob is not None:
            return [-1, 1][self.noob]

        if len(self.choices) > 0:
            return None

        if sum(self.hands[0] + self.hands[1]) == 0:
            return 0
        else:
            if self.turn % 2 == 0:
                return -1
            else:
                return 1

    def dump(self):
        """dump the current state
        """
        d = {
            'cards': self.cards,
            'remain': self.remain,
            'fields': self.fields,
            'bans': self.bans,
            'hands': self.hands,
            'turn': self.turn,
            'last_play': self.last_play,
            'noob': self.noob,
        }
        return json.dumps(d)

    def load(self, string):
        """load a state, reset cache
        """
        d = json.loads(string)
        self.cards = d['cards']
        self.remain = d['remain']
        self.fields = d['fields']
        self.bans = d['bans']
        self.hands = d['hands']
        self.turn = d['turn']
        self.last_play = d['last_play']
        self.noob = d['noob']
        # reset cache
        for key in self.cache.keys():
            n = self.turn
            while self.cache[key][n] is not None:
                self.cache[key][n] = None
                n += 1

        return self

    def train_data(self, pid=0):
        """train data
        0.p0 win, 1.p1 win
        """
        # result for p0, [1, 0, -1] = [win, draw, lose]
        result = self.result
        if result is None:
            return []

        data = []
        rec = self.records
        rec_num = len(rec)
        # for p0, we need state, action, reward, done, new_state
        # for p0, i % 2 == 0, result keeps
        for i in range(rec_num):
            if i % 2 == (1 - pid):
                continue
            if i + 2 >= rec_num:
                if pid == 1:
                    result = [-1, 0, 1][1 - result]
                if pid == 0:
                    result = [1, 0, -1][1 - result]
                if pid == self.noob:
                    result = result - 1

                data.append([rec[i][1], rec[i][0], result, 1, rec[i][1]])
            else:
                data.append([rec[i][1], rec[i][0], 0, 0, rec[i + 2][1]])

        return [np.hstack(d) for d in data]


if __name__ == '__main__':
    from tqdm import tqdm

    game = CardofSpirits()
    print('Test: rand vs rand')
    all_result = np.array([0, 0, 0])
    all_turns = np.array([0, 0, 0])

    episodes = 10000

    for i in tqdm(range(episodes), ascii=True):
        game.reset()
        for t in range(100):
            if game.result is None:
                cid = np.random.choice(game.choices)
                game.use(cid)
            else:
                break
        all_result[1 - game.result] += 1
        all_turns[1 - game.result] += t

    print('Win/Draw/Lose:', all_result / episodes)
    print('Mean turns:', all_turns / all_result)
