import numpy as np
layers = [[32, 256], [256, 256], [256, 256],
          [256, 256], [256, 1], [256, 13]]

weights = np.load("agent1-v0.npy")

w, b = [], []
k = 0
for row, col in layers:
    w.append(weights[0, k:k+row*col].reshape(row, col))
    k += row * col
    row = 1
    b.append(weights[0, k:k+row*col].reshape(row, col))
    k += row * col

for i, x in enumerate(w):
    np.savetxt("w%d.txt" % i, x)

for i, x in enumerate(b):
    np.savetxt("b%d.txt" % i, x)


class AgentTrained:
    def __init__(self, game, load_file, a_id=0):
        layers = [[32, 256], [256, 256], [256, 256],
                  [256, 256], [256, 1], [256, 13]]
        self.load(load_file, layers)

    def run_target(self, state):
        a = state.reshape(1, -1)
        for i in range(4):
            a = np.matmul(a, self.w[i]) + self.b[i]
            a[a <= 0] = 0
        val = np.matmul(a, self.w[4]) + self.b[4]
        adv = np.matmul(a, self.w[5]) + self.b[5]
        print(val, adv)
        return val + adv - np.mean(adv)

    def load(self, load_file, layers):
        weights = np.load(load_file)

        self.w, self.b = [], []
        k = 0
        for row, col in layers:
            self.w.append(weights[0, k:k+row*col].reshape(row, col))
            k += row * col
            row = 1
            self.b.append(weights[0, k:k+row*col].reshape(row, col))
            k += row * col

    def select_cid_best(self, state, choices):
        q = self.run_target(state.reshape(1, -1))
        return choices[np.argmax(q[0][choices])]


agent = AgentTrained(None, "agent1-v0.npy")
print(agent.run_target(np.zeros((1, 32))))
